#!/bin/bash

id=1
idstring=$(printf '%03d' ${id})
while [ -f "/etc/systemd/system/pac-${idstring}.service" ]; do
  echo "pac-${idstring}:"
  pacglobal-cli-${idstring} "$@"
  id=$((id + 1))
  idstring=$(printf '%03d' ${id})
done