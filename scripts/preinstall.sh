echo "####### Upgrading machine versions"
apt update && apt upgrade -y >/dev/null 2>&1
echo "####### installinging additional dependencies and docker if needed"
if ! apt-get install -y docker.io apt-transport-https curl fail2ban unattended-upgrades ufw dnsutils jq >/dev/null; then
  echo "Install cannot be completed successfully see errors above!"
fi

# Create swapfile if less then 2GB memory
totalmem=$(free -m | awk '/^Mem:/{print $2}')
totalswp=$(free -m | awk '/^Swap:/{print $2}')
totalm=$(($totalmem + $totalswp))
if [ $totalm -lt 4000 ]; then
  echo "Server memory is less then 2GB..."
  if ! grep -q '/swapfile' /etc/fstab; then
    echo "Creating a 2GB swapfile..."
    fallocate -l 2G /swapfile
    chmod 600 /swapfile
    mkswap /swapfile
    swapon /swapfile
    echo '/swapfile none swap sw 0 0' >>/etc/fstab
  fi
fi

#####################
echo "####### Creating the docker mount directories..."
mkdir -p /mnt/pac/ /opt/pac/

echo "####### Adding pac control directories to path"
if [[ $(cat ~/.bashrc | grep pac | wc -l) -eq 0 ]]; then
  echo 'export PATH=$PATH:/opt/pac' >>~/.bashrc
fi
source ~/.bashrc

docker login registry.gitlab.com -u pac-pub -p fzxLG9DGzhznyWxkJ6oB >/dev/null 2>&1

## Download the real scripts here
wget https://gitlab.com/pacglobal/pac-docker/raw/master/scripts/install_pac.sh -O /opt/pac/install_pac.sh
wget https://gitlab.com/pacglobal/pac-docker/raw/master/scripts/multi_install_pac.sh -O /opt/pac/multi_install_pac.sh
wget https://gitlab.com/pacglobal/pac-docker/raw/master/scripts/pac_control.sh -O /opt/pac/pac_control.sh
wget https://gitlab.com/pacglobal/pac-docker/raw/master/scripts/pac_all_params.sh -O /opt/pac/pac_all_params.sh
wget https://gitlab.com/pacglobal/pac-docker/raw/master/scripts/uninstall_pac.sh -O /opt/pac/uninstall_pac.sh
wget https://gitlab.com/pacglobal/pac-docker/raw/master/scripts/pac_mn_status.sh -O /opt/pac/pac_mn_status.sh
wget https://gitlab.com/pacglobal/pac-docker/raw/master/scripts/pac-cli.sh -O /opt/pac/pac-cli.sh
chmod +x /opt/pac/*.sh

echo
echo "####### SERVER INSTALLED COPY AND PASTE THE FOLLOWING COMMAND TO INSTALL YOUR FIRST NODE"
echo "source ~/.bashrc && install_pac.sh"