#!/bin/bash

set -e

## GET IPv4/6 Address
IP=$(curl -s ipinfo.io/ip)
echo "####### Your IP: $IP"

## check for nodes now
# Get current bitg node number
id=1
idstring=$(printf '%03d' ${id})
while [ -f "/etc/systemd/system/bitg-${idstring}.service" ]; do
  id=$((id + 1))
  idstring=$(printf '%03d' ${id})
done
rpcport=18${idstring}
port=9${idstring}
rpcport=10${idstring}

echo "####### creating /etc/systemd/system/bitg-${idstring}.service"
IMAGE=registry.gitlab.com/bitgreen/bitg-docker:latest
cat <<EOF >/etc/systemd/system/bitg-${idstring}.service
[Unit]
Description=BITG Daemon Container ${idstring}
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=10m
Restart=always
ExecStartPre=-/usr/bin/docker stop bitg-${idstring}
ExecStartPre=-/usr/bin/docker rm  bitg-${idstring}
# Always pull the latest docker image
ExecStartPre=/usr/bin/docker pull ${IMAGE}
ExecStop=/usr/bin/docker exec bitg-${idstring} /opt/app/bitgreen-cli stop
ExecStart=/usr/bin/docker run --rm -p ${port}:${port} -p ${rpcport}:${rpcport} -v /mnt/bitg/${idstring}:/root/.bitgreen --name bitg-${idstring} ${IMAGE}
[Install]
WantedBy=multi-user.target
EOF
systemctl enable bitg-${idstring}.service
systemctl daemon-reload

echo "####### creating /mnt/bitg/${idstring}/bitgreen.conf"
mkdir -p /mnt/bitg/${idstring}
cat <<EOF >/mnt/bitg/${idstring}/bitgreen.conf
rpcuser=user
rpcpassword=asdd3rascsar
rpcport=${rpcport}
rpcallowip=127.0.0.1
server=1
# Docker doesn't run as daemon
daemon=0
listen=1
txindex=1
logtimestamps=1
#
port=${port}
externalip=${IP}

addnode=45.63.2.241
addnode=104.156.249.186
addnode=140.82.12.18
addnode=149.28.239.44
addnode=149.28.61.99
EOF

systemctl start bitg-${idstring}

echo "####### adding control scripts"
cat <<EOF >/opt/bitg/bitgreen-cli-${idstring}
#!/bin/bash
docker exec bitg-${idstring} /opt/app/bitgreen-cli \$@
EOF
chmod +x /opt/bitg/bitgreen-cli-${idstring}

cat <<EOF >/opt/bitg/chainparams-${idstring}.sh
#!/bin/bash
echo
echo "### YOUR PARAMETERS!"
cat /mnt/bitg/${idstring}/bls.json |  jq '. += {"ip":"${IP}:${port}", "node":"$(hostname)-bitg-${idstring}"}'
EOF
chmod +x /opt/bitg/chainparams-${idstring}.sh

count=1
until bitgreen-cli-${idstring} masternode status 2> /dev/null; do
  echo "##### Waiting for start. Your parameters will appear shortly (1-2 mins)! (looping $count)"
  sleep 10
  count=$((count + 1))
  if [ $count -gt 30 ]; then
    echo "##### Server seems overloaded cannot get chainparams try again later with 'sh /opt/bitg/chainparams-${idstring}.sh'"
    break
  fi
done
sleep 5

sh /opt/bitg/chainparams-${idstring}.sh

echo "#--# type 'source ~/.bashrc' after that you can use the bitgreen-cli-${idstring} i.E. 'bitgreen-cli-${idstring} masternode status'"
